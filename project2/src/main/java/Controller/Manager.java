package Controller;
import Models.Client;
import Models.Coada;
import Models.ComparableArrival;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Manager implements Runnable {
    private int maxTimeArrival;
    private int minTimeArrival;
    private int minTimeService;
    private int maxTimeService;
    private int nrClienti;
    private int nrCozi;
    private int timeSimulation;
    private AtomicInteger currentTime = new AtomicInteger();
    private String fileOut;

    double waitingTimeAvg;
    private List<Coada> queues = new ArrayList<Coada>(nrCozi);
    private List<Client> clients = new ArrayList<Client>(nrCozi);

    public void sortClienti() {
        ComparableArrival comparableArrival = new ComparableArrival();
        Collections.sort(clients, comparableArrival);
    }


    public Manager(int maxTimeArrival, int minTimeArrival, int maxTimeService, int minTimeService, int nrClienti, int nrCozi, int timeSimulation,String fileOut) throws IOException {
        this.maxTimeArrival = maxTimeArrival;
        this.minTimeArrival = minTimeArrival;
        this.maxTimeService = maxTimeService;
        this.minTimeService = minTimeService;
        this.nrClienti = nrClienti;
        this.nrCozi = nrCozi;
        this.timeSimulation = timeSimulation;
        generateNClients();
        for (int i = 0; i < nrCozi; i++) {
            queues.add(new Coada(i, timeSimulation, currentTime));
            new Thread(queues.get(i)).start();
        }
        currentTime.set(0);
        this.fileOut=fileOut;
    }


    public void generateNClients() {
        int tarrival = 0;
        int tservice = 0;
        for (int i = 0; i < nrClienti; i++) {
            Random rnd = new Random();
            tarrival = minTimeArrival + rnd.nextInt(maxTimeArrival - minTimeArrival);
            tservice = minTimeService + rnd.nextInt(maxTimeService - minTimeService);
            Client client = new Client(i, tarrival, tservice);
            clients.add(client);
        }
        sortClienti();

    }

    public void run() {
        File myFile = new File(fileOut);
        try {
            if (myFile.createNewFile()) {
                System.out.println("File created: " + myFile.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(myFile.getName());

            while (!clients.isEmpty() && (currentTime.intValue() < timeSimulation)) {

                if (clients.get(0).getTarrival() <= currentTime.intValue()) {
                    for (int i = 0; i < nrCozi; i++) {
                        if (queues.get(i).isEmpty() == true) {
                            queues.get(i).addClient(clients.get(0));
                            break;
                        }
                    }
                    clients.remove(0);
                    nrClienti--;
                }

                myWriter.write("Current Time: " + currentTime+"\n");
                for (int i = 0; i < nrClienti; i++) {
                   myWriter.write("(" + clients.get(i).getId() + "," + clients.get(i).getTarrival() + "," + clients.get(i).getTservice() + ")\n");
                }
                for (int i = 0; i < nrCozi; i++) {
                    if (!queues.get(i).isEmpty()) {
                        myWriter.write("Queues" +i);
                        for(Client m:queues.get(i).getClienti()){
                           myWriter.write("("+m.getId()+","+m.getTarrival()+","+m.getTservice()+")\n");
                        }
                    } else myWriter.write("Queues" + i + ":Empty\n");
                }
                currentTime.incrementAndGet();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            for (int i = 0; i < nrCozi; i++) {
                waitingTimeAvg += queues.get(i).getReportWaiting();
            }
            waitingTimeAvg /= nrCozi;
            myWriter.write("Wainting time:" + waitingTimeAvg);

            myWriter.flush();
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }
}




