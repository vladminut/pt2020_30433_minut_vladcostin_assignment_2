package Controller;

//IMPORTANT: run with “java -cp PT2020_30433_Minut_VladCostin_Assignment2.jar Controller.Controll in-test-1.txt out-test-1”

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Controll {
    private int numberClients;
    private int numberQueues;
    private int tSimulation;
    private int tArrivalMin;
    private int tArrivalMax;
    private int tServiceMin;
    private int tServiceMax;

    public static void main(String[] args) throws IOException, NumberFormatException {
        Controll controll=new Controll();
        if(args.length!=0){
            controll.readFromFile(args[0]);
            Manager manager = new Manager(controll.gettArrivalMax(), controll.gettArrivalMin(), controll.gettServiceMax(), controll.gettServiceMin(), controll.getNumberClients(), controll.getNumberQueues(), controll.gettSimulation(), args[1]);
            Thread t = new Thread(manager);
            t.start();
        }
        else System.out.println("Argumente ");

    }

    public void readFromFile(String fileName){
        try{
            FileReader fileReader=new FileReader(fileName);
            BufferedReader reader=new BufferedReader(fileReader);
            String text=null;
            text=reader.readLine();
            numberClients=Integer.parseInt(text);
            text=reader.readLine();
            numberQueues=Integer.parseInt(text);
            text=reader.readLine();
            tSimulation=Integer.parseInt(text);
            text=reader.readLine();
            String[] valori=text.split(",");
            tArrivalMin=Integer.parseInt(valori[0]);
            tArrivalMax=Integer.parseInt(valori[1]);
            text=reader.readLine();
            String[] valori1=text.split(",");
            tServiceMin=Integer.parseInt(valori1[0]);
            tServiceMax=Integer.parseInt(valori1[1]);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getNumberClients() {
        return numberClients;
    }

    public int getNumberQueues() {
        return numberQueues;
    }

    public int gettArrivalMax() {
        return tArrivalMax;
    }

    public int gettArrivalMin() {
        return tArrivalMin;
    }

    public int gettServiceMax() {
        return tServiceMax;
    }

    public int gettServiceMin() {
        return tServiceMin;
    }

    public int gettSimulation() {
        return tSimulation;
    }
}
