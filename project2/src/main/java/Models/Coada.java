package Models;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Coada implements Runnable {
     private AtomicInteger timpAsteptare = new AtomicInteger();
     private BlockingQueue<Client> clienti;
     private int id;
     private int tSimulation;
     private int[] Count=new int[200] ;
     private double[] Waiting=new double[200];
     private AtomicInteger currentTime;

     public Coada(int id, int tSimulation,AtomicInteger currentTime) {
          clienti = new ArrayBlockingQueue<Client>(100);
          timpAsteptare.set(0);
          this.currentTime = currentTime;
          this.id=id;
          this.tSimulation=tSimulation;
     }

     public void addClient(Client client) {
          if (client.getTarrival() <= tSimulation){
               clienti.add(client);
               timpAsteptare.addAndGet(client.getTservice());

               Waiting[this.getId()] += this.timpAsteptare.intValue();
               Count[this.getId()] ++;

          }

     }

     public void removeClient(){
          timpAsteptare.addAndGet(-clienti.element().getTservice());
          clienti.remove();
     }

     public void run()
     {
          while(true) {
               if (clienti.size() >0) {
                    Client t = clienti.element();
                    try {
                         Thread.sleep(t.getTservice() * 1000);
                    } catch (InterruptedException e) {
                         e.printStackTrace();

                    }
                    removeClient();
               }

               else if(currentTime.intValue() >tSimulation ) break;
          }
     }


     public int getId() {
          return id;
     }


     public AtomicInteger getTimpAsteptare() {
          return timpAsteptare;
     }

     public BlockingQueue<Client> getClienti() {
          return clienti;
     }
     public boolean isEmpty(){
          return clienti.isEmpty();
     }

     public double getReportWaiting()
     {
          return this.Waiting[this.getId()]/this.Count[this.getId()];
     }

     public void printClients(){
          for(Client m:clienti){
               System.out.println("("+m.getId()+","+m.getTarrival()+","+m.getTservice()+")");
          }
     }
}
