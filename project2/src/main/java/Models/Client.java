package Models;

public class Client {
    private int id;
    private int tarrival;
    private int tservice;

    public Client(int id,int tarrival,int tservice){
        this.id=id;
        this.tarrival=tarrival;
        this.tservice=tservice;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTarrival(int tarrival) {
        this.tarrival = tarrival;
    }

    public int getTarrival() {
        return tarrival;
    }

    public void setTservice(int tservice) {
        this.tservice = tservice;
    }

    public int getTservice() {
        return tservice;
    }
}
