package Models;

import java.util.Comparator;

public class ComparableArrival implements Comparator<Client> {

        public int compare(Client o1, Client o2) {
            if(o1.getTarrival()==o2.getTarrival())
                return 0;
            else if(o1.getTarrival()>o2.getTarrival())
                return 1;
            else
                return -1;
        }
    }

